package example.com.receiverapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.widget.Toast;



public class MyReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        String userName = bundle.getString("userName");
        String password = bundle.getString("password");

        final Toast tag = Toast.makeText(context, "Intercepting from Receiver app (insecured version)\nUsername "+userName+"\n"+"Password "+password+"\n" ,Toast.LENGTH_SHORT);
        tag.show();
        new CountDownTimer(10000, 1000)
        {
            public void onTick(long millisUntilFinished) {tag.show();}
            public void onFinish() {tag.show();}

        }.start();

    }
}